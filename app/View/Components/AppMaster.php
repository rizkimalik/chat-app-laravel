<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AppMaster extends Component
{
    public $title= 'App Chat Mendawai';
    
    public $style = null;

    public $script = null;

    public function __construct()
    {
        $this->title = 'App Chat Mendawai';
    }

    public function render()
    {
        return view('components.app-master');
    }
}

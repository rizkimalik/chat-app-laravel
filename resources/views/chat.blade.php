<x-app-master>
    <x-conversation>
        <x-topbar />

        <!-- start chat conversation -->
        <div class="chat-conversation p-3 p-lg-4 " id="chat-conversation" data-simplebar>
            <ul class="list-unstyled chat-conversation-list" id="chat-conversation-list"></ul>
        </div>
        <!-- end chat conversation -->
    </x-conversation>
    <x-form-send />

    @php
    $session = session()->all();
    @endphp

    <x-slot:script>
        <script src="{{ asset('assets/js/init.js') }}"></script>
        {{-- <script src="{{ asset('assets/scripts/config.js') }}"></script> --}}
        <script>
            const session = @json($session);
            const formSend = document.querySelector("#chatinput-form");
            const message = document.querySelector("#chat-input");
            const conversation = document.querySelector("#chat-conversation-list");
            const submitbtn = document.querySelector("#submit-btn");
            const ActiveChat = localStorage.getItem('ActiveChat');
            const data_active = JSON.parse(ActiveChat);

            const firebaseConfig = {
                // apiKey: "AIzaSyBDRIiWOUF51w6m1dvdNtLkHehcjcv-C1o",
                // authDomain: "worktool-f50cb.firebaseapp.com",
                // projectId: "worktool-f50cb",
                // storageBucket: "worktool-f50cb.appspot.com",
                // messagingSenderId: "808725237125",
                // appId: "1:808725237125:web:33a8c8ad003e2df913c8ec"

                apiKey: "AIzaSyDG3DVEILnwTYdo1RPuCfaXRHoNRSyXnEM",
                authDomain: "web-chat-a7180.firebaseapp.com",
                projectId: "web-chat-a7180",
                storageBucket: "web-chat-a7180.appspot.com",
                messagingSenderId: "492304206438",
                appId: "1:492304206438:web:0dd178e41155a8133efba7",
            };

            firebase.initializeApp(firebaseConfig);

            const messaging = firebase.messaging();
            messaging.requestPermission()
            .then(function () {
                console.log("Notification permission granted.");
                return messaging.getToken();
                // return messaging.getToken({vapidKey: "BEuMvTP23k7cKq2w7ISif00fosMKK0TRFaQ6SuHrJvrV53U7qGEjbYHw_-7bnO08PeIU9urpYjjcVhYudUWw0ws"});
            })
            .then(function (token) {
                // console.log(token);
                localStorage.setItem('uuid', token);
                document.getElementById('from-token').value = token;
            })
            .catch(function (err) {
                console.log("Unable to get permission to notify.", err);
            });

            firebase.messaging().onMessage(function (payload) {
                const data = payload.data;
                onCheckReadyChat(data);
                localStorage.setItem('ActiveChat', JSON.stringify(data));

                conversation.innerHTML += 
                    `<li class="chat-list left">
                        <div class="conversation-list">
                            <div class="chat-avatar avatar-sm">
                                <div class="avatar-title bg-soft-primary text-primary rounded-circle">
                                    <i class="bx bxs-message-square-detail"></i>
                                </div>
                            </div>
                            <div class="user-chat-content">
                                <div class="ctext-wrap">
                                    <div class="ctext-wrap-content">
                                        <p class="mb-0 ctext-content">${data.message}</p>
                                    </div>
                                    <div class="dropdown align-self-start message-box-drop"> <a class="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bx bx-dots-vertical-rounded align-middle"></i> </a>
                                        <div class="dropdown-menu"> <a class="dropdown-item d-flex align-items-center justify-content-between reply-message" href="#" id="reply-message-0" data-bs-toggle="collapse" data-bs-target=".replyCollapse">Reply <i class="bx bx-share ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="modal" data-bs-target=".forwardModal">Forward <i class="bx bx-share-alt ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between copy-message" href="#" id="copy-message-0">Copy <i class="bx bx-copy text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i class="bx bx-bookmarks text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Mark as Unread <i class="bx bx-message-error text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between delete-item" href="#">Delete <i class="bx bx-trash text-muted ms-2"></i></a> </div>
                                    </div>
                                </div>
                                <div class="conversation-name">
                                    <small class="text-muted time">${h()}</small> 
                                    <span class="text-success check-message-icon"><i class="bx bx-check-double"></i></span>
                                </div>
                            </div>
                        </div>
                    </li>`;

                const {title, body, icon} = payload.notification;
                const notificationOption = {
                    body: body,
                    icon: icon
                };
                navigator.serviceWorker
                    .getRegistrations()
                    .then((registration) => {
                        registration[0].showNotification(`${title}`, notificationOption);
                    });
            });

            // if ready chat
            onCheckReadyChat(data_active);
            formSend.addEventListener("submit", async (e) => {
                e.preventDefault();

                const ActiveChat = localStorage.getItem('ActiveChat');
                const data = JSON.parse(ActiveChat);

                // const dataJson = JSON.stringify({
                //         "ReqId": "rid001",
                //         "ReqCode": "code001",
                //         "Email": session.email,
                //         "FromName": session.name,
                //         "FromToken": localStorage.getItem('uuid'),
                //         "ToToken": uuid_agent,
                //         "FlagTo": "customer",
                //         "Message": message.value,
                //         "ImageUrl": "N/A"
                //     });

                const options = {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        "uuid_agent":data.uuid_agent,
                        "uuid_customer":localStorage.getItem('uuid'),
                        "uuid":data.uuid_agent,
                        "name":session.name,
                        "customer_id":data.customer_id,
                        "chat_id": data.chat_id,
                        "email":session.email,
                        "message":message.value,
                        "flag_to":"customer",
                        "agent_handle": data.agent_handle
                    }),
                    redirect: 'follow'
                }

                // await fetch("/chat-app/api/sendmessage", options)
                // await fetch("https://mendawai.test/api-chat/sendchat.php", options)
                await fetch("https://mendawai.test/api-chat/send_message.php", options)
                .then(response => response.json())
                .then(result => (result) => {
                    console.log(result)
                    if (result.success) {
                        alert('sukses kirim')
                    } 
                    else {
                        alert('Error send message.')
                    }
                })
                .catch(error => console.log('error', error));

                conversation.innerHTML += 
                `<li class="chat-list right">
                    <div class="conversation-list">
                        <div class="user-chat-content">
                            <div class="ctext-wrap">
                                <div class="ctext-wrap-content">
                                    <p class="mb-0 ctext-content">${message.value}</p>
                                </div>
                                <div class="dropdown align-self-start message-box-drop"> <a class="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="bx bx-dots-vertical-rounded align-middle"></i> </a>
                                    <div class="dropdown-menu"> <a class="dropdown-item d-flex align-items-center justify-content-between reply-message" href="#" data-bs-toggle="collapse" data-bs-target=".replyCollapse">Reply <i class="bx bx-share ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="modal" data-bs-target=".forwardModal">Forward <i class="bx bx-share-alt ms-2 text-muted"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between copy-message" href="#">Copy <i class="bx bx-copy text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i class="bx bx-bookmarks text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">Mark as Unread <i class="bx bx-message-error text-muted ms-2"></i></a> <a class="dropdown-item d-flex align-items-center justify-content-between delete-item" href="#">Delete <i class="bx bx-trash text-muted ms-2"></i></a> </div>
                                </div>
                            </div>
                            <div class="conversation-name">
                                <small class="text-muted time">${h()}</small> 
                                <span class="text-success check-message-icon"><i class="bx bx-check"></i></span>
                            </div>
                        </div>
                    </div>
                </li>`;
                message.value = "";
                
            });

            function h() {
                var e = 12 <= (new Date).getHours() ? "pm" : "am",
                    t = 12 < (new Date).getHours() ? (new Date).getHours() % 12 : (new Date).getHours(),
                    a = (new Date).getMinutes() < 10 ? "0" + (new Date).getMinutes() : (new Date).getMinutes();
                return t < 10 ? "0" + t + ":" + a + " " + e : t + ":" + a + " " + e
            }

            function onCheckReadyChat(item) {
                document.getElementById('agent_name').innerHTML = item.agent_handle;
                if (item) {
                    submitbtn.disabled = false;
                    message.disabled = false;
                }
                else {
                    submitbtn.disabled = true;
                    message.disabled = true;
                }
            }
        </script>
    </x-slot:script>
</x-app-master>
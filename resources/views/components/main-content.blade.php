<div class="layout-wrapper d-lg-flex">
    <div class="w-100 overflow-hidden">
        <div class="user-chat-overlay"></div>
        <div class="chat-content d-lg-flex ">
            <div class="w-100 overflow-hidden position-relative">
                {{ $slot }}
            </div>

            <x-user-profile />
        </div>
    </div>

    <x-modal />
</div>
<div class="p-3 p-lg-4 user-chat-topbar">
    <div class="row align-items-center">
        <div class="col-sm-4 col-8">
            <div class="d-flex align-items-center">
                <div class="flex-grow-1 overflow-hidden">
                    <div class="d-flex align-items-center">
                        <div
                            class="flex-shrink-0 avatar-sm online align-self-center me-3 ms-0">
                            <div class="avatar-title bg-soft-primary text-primary rounded-circle">
                                <i class="bx bxs-message-square-dots"></i>
                            </div>
                            <span class="user-status"></span>
                        </div>
                        <div class="flex-grow-1 overflow-hidden">
                            <h6 class="text-truncate mb-0 font-size-18"><a href="#"
                                    {{-- class="user-profile-show text-reset"><span id="name">{{ session('name') }}</span></a></h6> --}}
                                    class="user-profile-show text-reset"><span id="agent_name"></span></a></h6>
                            <p class="text-truncate text-muted mb-0"><small>Online</small></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-sm-8 col-4">
            <ul class="list-inline user-chat-nav text-end mb-0">
                {{-- <li class="list-inline-item">
                    <div class="dropdown">
                        <button class="btn nav-btn dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="bx bx-search"></i>
                        </button>
                        <div class="dropdown-menu p-0 dropdown-menu-end dropdown-menu-lg" style="">
                            <div class="search-box p-2">
                                <input type="text" class="form-control" placeholder="Search.." id="searchChatMessage">
                            </div>
                        </div>
                    </div>
                </li> --}}
                <li class="list-inline-item d-none d-lg-inline-block me-2 ms-0">
                    <button type="button" class="btn nav-btn" data-bs-toggle="modal"
                        data-bs-target=".audiocallModal">
                        <i class='bx bxs-phone-call'></i>
                    </button>
                </li>

                <li class="list-inline-item d-none d-lg-inline-block me-2 ms-0">
                    <button type="button" class="btn nav-btn" data-bs-toggle="modal"
                        data-bs-target=".videocallModal">
                        <i class='bx bx-video'></i>
                    </button>
                </li>

                <li class="list-inline-item d-none d-lg-inline-block me-2 ms-0">
                    <button type="button" class="btn nav-btn user-profile-show">
                        <i class='bx bxs-info-circle'></i>
                    </button>
                </li>

                <li class="list-inline-item">
                    <div class="dropdown">
                        <button class="btn nav-btn dropdown-toggle" type="button"
                            data-bs-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                            <i class='bx bx-dots-vertical-rounded'></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-end">
                            <a class="dropdown-item d-flex justify-content-between align-items-center d-lg-none user-profile-show"
                                href="#">View Profile <i class="bx bx-user text-muted"></i></a>
                            <a class="dropdown-item d-flex justify-content-between align-items-center d-lg-none"
                                href="#" data-bs-toggle="modal"
                                data-bs-target=".audiocallModal">Audio <i
                                    class="bx bxs-phone-call text-muted"></i></a>
                            <a class="dropdown-item d-flex justify-content-between align-items-center d-lg-none"
                                href="#" data-bs-toggle="modal"
                                data-bs-target=".videocallModal">Video <i
                                    class="bx bx-video text-muted"></i></a>
                            <a class="dropdown-item d-flex justify-content-between align-items-center"
                                href="#">Archive <i class="bx bx-archive text-muted"></i></a>
                            <a class="dropdown-item d-flex justify-content-between align-items-center"
                                href="#">Muted <i
                                    class="bx bx-microphone-off text-muted"></i></a>
                            <a class="dropdown-item d-flex justify-content-between align-items-center"
                                href="{{ route('login') }}">Delete <i class="bx bx-trash text-muted"></i></a>

                            {{-- <form method="POST" action="{{ route('logout') }}">
                                @csrf
                                <a class="dropdown-item d-flex justify-content-between align-items-center"
                                href="{{ route('logout') }}" onclick="event.preventDefault();this.closest('form').submit();">Delete <i class="bx bx-trash text-muted"></i></a>
                            </form> --}}
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    {{-- <div class="alert alert-warning alert-dismissible topbar-bookmark fade show p-1 px-3 px-lg-4 pe-lg-5 pe-5"
        role="alert">
        <div class="d-flex align-items-start bookmark-tabs">
            <div class="tab-list-link">
                <a href="#" class="tab-links" data-bs-toggle="modal"
                    data-bs-target=".pinnedtabModal">10 Pinned</a>
            </div>
            <div>
                <a href="#" class="tab-links border-0 px-3" data-bs-toggle="tooltip"
                    data-bs-trigger="hover" data-bs-placement="bottom" title="Add Bookmark"><i
                        class="ri-add-fill align-middle"></i> hahs</a>
            </div>
        </div>
        <button type="button" class="btn-close" data-bs-dismiss="alert"
            aria-label="Close"></button>
    </div> --}}

</div>